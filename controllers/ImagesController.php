<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\services\ImageProcessor;
use app\models\ImageForm;

class ImagesController extends Controller
{
    public function actionIndex()
    {
        $model = new ImageForm();

        return $this->render('index', ['model' => $model]);
    }

    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request= \Yii::$app->request;

        $processor = new ImageProcessor($request);

        if ($processor->run()) {
            return [
                'imageFilePath' => $processor->getImageFilePath(),
                'errors' => [],
            ];
        }
        
        return [
            'errors' => $processor->getErrors(),
        ];
    }

}
