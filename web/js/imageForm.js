var imageForm = function() {
	this.bindEvents = function() {
		var self = this;

		$('#image-file-form input[type="file"]').on('change', function(e){
			var file = e.target.files.item(0);
            var reader = new FileReader();
            var form = $(this).parents('form:first');
            reader.onload = function(e) {
            	var imageHtml = '<img class="thumbnail" src="' + e.target.result + '"/>';
                form.find('.preview').html(imageHtml);
            };

            $('#image-file-form-result').hide();

            reader.readAsDataURL(file);
		});

	    $('#image-file-form').on('submit', function(e) {
	        var form = $(this);
	        var data = new FormData(form.get(0));

	        form.addClass('js-loading');
	        form.attr('disabled', true);

	        e.preventDefault();
	        e.stopPropagation();

	        console.log('Sending data');
	        $('#image-file-form-result').html('');

	        $.ajax({
	            url: form.attr('action'),
	            method: form.attr('method'),
	            data: data,
				processData: false,
    			contentType: false,
	            success: function(data, textStatus, jqXHR) {
	            	console.log('Success');
	            	console.log(data);
	            
	                if (data.errors.length > 0) {
	                	var errors = data.errors;
	                	
	                	for (i in errors) {
	                		$('#image-file-form-result').append('<p>' + errors[i] + '</p>');
	                	}

	                } else {
	                	var r = Math.random();
	                	var imagePath = data.imageFilePath;
	                	var imageHtml = '<img class="thumbnail" src="./' + imagePath + '?v' + r + '"/>';

	                	$('#image-file-form-result').html(imageHtml);
	                }

	                $('#image-file-form-result').show();
	                form.removeClass('js-loading');
	                form.removeAttr('disabled');
	            },
	            error: function() {
	                form.removeClass('js-loading');
	                form.removeAttr('disabled');
	            }
	        });

	        return false;
	    });
	}

	this.bindEvents();
};

$(document).ready(function(){
	new imageForm();
});