<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Images';
?>

<div class="row images-index">
	<div class="col-md-6">
		<h1>Загрузите изображение</h1>

        <?php $form = ActiveForm::begin([
        	'action' => Url::toRoute(['images/create']),
        	'options' => ['enctype' => 'multipart/form-data'],
        	'id' => 'image-file-form',
        ]); ?>
        	<div class="loading">Идет обработка...</div>

        	<div class="form-fields">

	        	<div class="preview"></div>
	            
	            <?= $form->field($model, 'imageFile')->fileInput() ?>

	            <div class="form-group">
	                <?= Html::submitButton('Обработать', ['class' => 'btn btn-primary']) ?>
	            </div>
	        </div>

        <?php ActiveForm::end(); ?>
	</div>

	<div class="col-md-6">
		<h1>Результат</h1>
		<div id="image-file-form-result" class="preview">
			<p>Сначала загрузите изображение</p>
		</div>
	</div>
</div>