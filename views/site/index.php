<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Images';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute(['images/index']) ?>">Перейти к обработке изображений</a></p>
    </div>

</div>
