<?php
namespace app\services;

use app\models\ImageForm;
use yii\web\UploadedFile;

class ImageProcessor
{
	protected $request;
	protected $imageFilePath;
	protected $errors = [];

	public function __construct($request)
	{
		$this->request = $request;
		$this->errors = [];
	}

	public function run()
	{
		try {
			$this->processFilters();
		} catch(\Exception $e) {
			$this->errors[] = $e->getMessage();

			return false;
		}

		return true;
	}

	public function getImageFilePath()
	{
		return $this->imageFilePath;
	}

	public function getErrors()
	{
		return $this->errors;
	}

	protected function processFilters()
	{
		$form = new ImageForm();
		$form->imageFile = UploadedFile::getInstance($form, 'imageFile');

		if (!$form->validate()) {
			throw new \Exception('Invalid form data');
		}

		$file = $form->imageFile;

		$image = $this->getImageObject($file);

		if ($image == null) {
			throw new \Exception('Unrecognized file type');
		}


		imagefilter($image, IMG_FILTER_COLORIZE, 20, 15, 15, 100);
		imagefilter($image, IMG_FILTER_CONTRAST, -15);
		imagefilter($image, IMG_FILTER_BRIGHTNESS, 30);

		$filePath = 'uploads/image.jpg';

		imagejpeg($image, $filePath, 100);

		$this->setImageFilePath($filePath);

		return $this;
	}

	protected function getImageObject($file)
	{
		if ($file->extension == 'png') {
			return $im = imagecreatefrompng($file->tempName);
		}

		if ($file->extension == 'jpg'
			|| $file->extension == 'jpeg'
		) {
			return $im = imagecreatefromjpeg($file->tempName);
		}

		return null;
	}
	protected function setImageFilePath($imageFilePath)
	{
		$this->imageFilePath = $imageFilePath;
	}
}